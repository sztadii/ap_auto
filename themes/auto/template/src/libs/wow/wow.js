var wow;

wow = new function () {

  //private vars
  var wow;

  //binds events
  $(document).ready(function () {
    $('body').imagesLoaded({background: true}).done(function () {
      setTimeout(function () {
        wow = new WOW().init();
      }, 0);
    });
  });
};